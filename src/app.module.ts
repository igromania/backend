import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {ConnectedComputersService} from './core/services/computers/connected-computers.service';
import {ClientComputersGateway} from './ws-gateways/client-computers-gateway';
import {WebClientsGateway} from './ws-gateways/web-clients-gateway';
import {TypeOrmModule} from '@nestjs/typeorm';
import {DatabaseProvider} from './core/database/database.provider';
import {ComputerModule} from './core/database/entities/computer/computer.module';
import {ComputersService} from './core/database/entities/computer/computers.service';
import {UsersController} from './core/controllers/users/users.controller';
import {UserModule} from './core/database/entities/user/user.module';
import {UsersService} from './core/database/entities/user/users.service';
import {RolesGuard} from './ws-gateways/guards/roles-guard';
import {APP_GUARD} from '@nestjs/core';
import { AuthService } from './core/services/auth/auth.service';
import {TariffIntervalModule} from './core/database/entities/tariff-interval/tariff-interval.module';
import {TariffModule} from './core/database/entities/tariff/tariff.module';
import {ComputerSessionModule} from './core/database/entities/computer-session/computer-session.module';
import { GameSessionsService } from './core/services/game-sessions/game-sessions.service';
import {TariffService} from './core/database/entities/tariff/tariff.service';

@Module({
    imports: [
        TypeOrmModule.forRoot(DatabaseProvider),
        ComputerModule,
        UserModule,
        TariffIntervalModule,
        TariffModule,
        ComputerSessionModule,
    ],
    controllers: [AppController, UsersController],
    providers: [
        AppService,
        ConnectedComputersService,
        ClientComputersGateway,
        WebClientsGateway,
        ComputersService,
        AuthService,
        UsersService,
        {
            provide: APP_GUARD,
            useClass: RolesGuard,
        },
        GameSessionsService,
        TariffService,
    ],
})
export class AppModule {
}
