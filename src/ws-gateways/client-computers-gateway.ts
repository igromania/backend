import {OnGatewayDisconnect, SubscribeMessage, WebSocketGateway, WebSocketServer, WsResponse} from '@nestjs/websockets';
import {Client, Server} from 'socket.io';
import {ConnectedComputersService} from '../core/services/computers/connected-computers.service';
import {ConnectedComputer} from '../core/services/computers/models/connected-computer';
import {ComputersService} from '../core/database/entities/computer/computers.service';
import {UseGuards} from '@nestjs/common';
import {OnClientComputerConnectGuard} from './guards/on-client-computer-connect-guard.service';

@WebSocketGateway(3334)
export class ClientComputersGateway implements OnGatewayDisconnect {
    @WebSocketServer()
    server: Server;

    constructor(private readonly connectedComputersService: ConnectedComputersService,
                private readonly computersService: ComputersService) { }

    handleDisconnect(client: any | Client): any {
        this.connectedComputersService.removeComputerBySocketId(client.id);
        return null;
    }

    @SubscribeMessage('identity')
    @UseGuards(OnClientComputerConnectGuard)
    async identity(client: any | Client) {
        const computer = new ConnectedComputer(client);
        this.connectedComputersService.addComputer(computer);
    }
}
