import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {Observable} from 'rxjs';
import {JwtPayload} from '../../core/models/jwt-payload';
import * as jwt from 'jsonwebtoken';
import {UsersService} from '../../core/database/entities/user/users.service';

@Injectable()
export class IdentityWsUserGuard implements CanActivate {
    constructor(private readonly usersService: UsersService) {

    }

    async canActivate(
        context: ExecutionContext,
    ) {
        const client = context.switchToWs().getClient();
        const cookies: string[] = client.handshake.headers.cookie.split('; ');
        const authToken = cookies.find(cookie => cookie.startsWith('token')).split('=')[1];
        const jwtPayload: JwtPayload = jwt.verify(authToken, 'S8nOqWlS2P1SJu') as JwtPayload;
        const user = await this.usersService.getByLogin(jwtPayload.login);
        context.switchToWs().getData().user = user;
        return true;
    }
}
