import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {Observable} from 'rxjs';
import {ComputersService} from '../../core/database/entities/computer/computers.service';
import {ConnectedComputer} from '../../core/services/computers/models/connected-computer';

@Injectable()
export class OnClientComputerConnectGuard implements CanActivate {
    constructor(private readonly computersService: ComputersService) {

    }

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const computer = new ConnectedComputer(context.getArgs()[0]);
        return this.computersService.findByIp(computer.ip) !== null;
    }
}
