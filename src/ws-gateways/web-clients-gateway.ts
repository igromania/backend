import {OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage, WebSocketGateway, WebSocketServer} from '@nestjs/websockets';
import {Client, Server} from 'socket.io';
import {ConnectedComputersService} from '../core/services/computers/connected-computers.service';
import {WebSocketMessage} from 'rxjs/internal/observable/dom/WebSocketSubject';
import {UseGuards} from '@nestjs/common';
import {IdentityWsUserGuard} from './guards/identity-ws-user-guard.service';
import {DataWithUser} from '../core/models/data-with-user';
import {ComputersService} from '../core/database/entities/computer/computers.service';

@WebSocketGateway(8080)
export class WebClientsGateway implements OnGatewayConnection, OnGatewayDisconnect {
    @WebSocketServer()
    server: Server;

    private clients: Client[] = [];

    constructor(private readonly connectedComputerService: ConnectedComputersService,
                private readonly computersService: ComputersService) {
        connectedComputerService.computers$.subscribe(computers => {
            this.clients.map(c => (c as any).emit('connected-computers', computers.map(
                computer => computer.getSummaryInformation(),
            )));
        });
    }

    handleConnection(client: any | Client, ...args: any[]): any {
        this.clients.push(client);
        return null;
    }

    handleDisconnect(client: any | Client): any {
        this.clients = this.clients.filter(c => c.id !== client.id);
    }

    @SubscribeMessage('identity')
    @UseGuards(IdentityWsUserGuard)
    authSocket(client: any | Client, data: DataWithUser) {
        if (['owner', 'admin'].indexOf(data.user.role) !== -1) {
            client.join('admins');
            this.sendComputersStatusToAdmins();
        }
    }

    private broadcastToAdmins(eventName: string, data: any) {
        this.server.to('admins').emit(eventName, data);
    }

    private async sendComputersStatusToAdmins() {
        const computers = await this.computersService.findAll();
        this.broadcastToAdmins('computers-list', computers);
    }
}
