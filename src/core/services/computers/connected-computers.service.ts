import {Injectable} from '@nestjs/common';
import {BehaviorSubject} from 'rxjs';
import {ConnectedComputer} from './models/connected-computer';
import {ComputersService} from '../../database/entities/computer/computers.service';

@Injectable()
export class ConnectedComputersService {
    public computers$: BehaviorSubject<ConnectedComputer[]> = new BehaviorSubject([]);

    constructor(private readonly computersService: ComputersService) { }

    public addComputer(computer: ConnectedComputer) {
        const clients = this.computers$.getValue();
        clients.push(computer);
        this.computers$.next(clients);
    }

    public removeComputerBySocketId(socketId: string) {
        let computers = this.computers$.getValue();
        computers = computers.filter(computer => computer.client.id !== socketId);
        this.computers$.next(computers);
    }
}
