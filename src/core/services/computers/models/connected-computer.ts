import {Client} from 'socket.io';
import * as ipaddr from 'ipaddr.js';

export class ConnectedComputer {
    public ip?: string;
    public client: Client;

    constructor(client: Client) {
        this.client = client;

        this.ip = ipaddr.process(client.conn.remoteAddress).toNormalizedString();
    }

    public emit(eventName: string, message: any) {
        (this.client as any).emit(eventName, message);
    }

    public getSummaryInformation() {
        return {
            ip: this.ip,
        };
    }
}
