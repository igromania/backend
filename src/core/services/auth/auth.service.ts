import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import {JwtPayload} from '../../models/jwt-payload';

@Injectable()
export class AuthService {
    constructor() { }

    public createToken(payload: JwtPayload) {
        return jwt.sign(payload, 'S8nOqWlS2P1SJu');
    }
}
