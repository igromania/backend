import {Test, TestingModule} from '@nestjs/testing';
import {GameSessionsService} from './game-sessions.service';
import {TariffService} from '../../database/entities/tariff/tariff.service';
import {TariffModule} from '../../database/entities/tariff/tariff.module';
import {Tariff} from '../../database/entities/tariff/tariff.entity';
import {TypeOrmModule} from '@nestjs/typeorm';
import {DatabaseProvider} from '../../database/database.provider';

describe('GameSessionsService', () => {
    let service: GameSessionsService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [TariffModule, TypeOrmModule.forRoot(DatabaseProvider)],
            providers: [GameSessionsService, TariffService],
        }).compile();

        service = module.get<GameSessionsService>(GameSessionsService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    // it('should calc game session price for tariff', async () => {
    //     const sessionTime = 60;
    //     jest.spyOn(service, 'calcPriceForTariffBySessionTime').mockImplementation(() => sessionTime);
    //     expect(await service.calcPriceForTariffBySessionTime()).toBe(result);
    //     // expect(sessionTime).toBe(sessionTime);
    // });
});
