import {Column, Entity, ManyToOne, PrimaryGeneratedColumn, Table, Unique} from 'typeorm';
import {Tariff} from '../tariff/tariff.entity';

@Entity()
export class TariffInterval {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: 'time'})
    from: string;

    @Column({type: 'time'})
    to: string;

    @Column({type: 'integer'})
    price: number;

    @ManyToOne(type => Tariff, tariff => tariff.intervals)
    tariff: Tariff[];
}
