import {Module} from '@nestjs/common';
import {TariffInterval} from './tariff-interval.entity';
import {TypeOrmModule} from '@nestjs/typeorm';

@Module({
    imports: [
        TypeOrmModule.forFeature([TariffInterval])],
    providers: [],
    controllers: [],
})
export class TariffIntervalModule {
}
