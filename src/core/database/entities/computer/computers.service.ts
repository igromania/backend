import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import {Computer} from './computer.entity';
import {InjectRepository} from '@nestjs/typeorm';

@Injectable()
export class ComputersService {
    constructor(
        @InjectRepository(Computer)
        private readonly computerRepository: Repository<Computer>,
    ) {}

    async findAll(): Promise<Computer[]> {
        return await this.computerRepository.find();
    }

    async findByIp(ip: string): Promise<Computer> {
        return await this.computerRepository
            .createQueryBuilder('computer')
            .where('ip = :ip', { ip })
            .getOne();
    }
}
