import {Column, Entity, PrimaryGeneratedColumn, Table, Unique} from 'typeorm';

@Entity()
@Unique(['ip'])
export class Computer {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 12 })
    ip: string;

    @Column({ length: 100 })
    name: string;

    @Column({ type: 'integer', default: 0 })
    status: number;
}
