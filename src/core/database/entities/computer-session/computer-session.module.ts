import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ComputersController} from '../../../controllers/computers/computers.controller';
import {ComputerSession} from './computer-session.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ComputerSession])],
    providers: [],
    controllers: [],
})
export class ComputerSessionModule {}
