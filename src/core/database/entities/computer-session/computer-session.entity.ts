import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Tariff} from '../tariff/tariff.entity';

@Entity()
export class ComputerSession {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: 'datetime'})
    from: string;

    @Column({type: 'datetime'})
    to: string;

    @ManyToOne(type => Tariff, tariff => tariff.id)
    tariff: Tariff;

    @Column({type: 'integer'})
    cost: number;

    @Column({type: 'boolean'})
    active: boolean;
}
