import {Column, Entity, OneToMany, PrimaryGeneratedColumn, Unique} from 'typeorm';
import {TariffInterval} from '../tariff-interval/tariff-interval.entity';

@Entity()
@Unique(['name'])
export class Tariff {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({length: 100})
    name?: string;

    @OneToMany(type => TariffInterval, interval => interval.tariff)
    intervals: TariffInterval[];
}
