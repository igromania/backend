import {Test, TestingModule} from '@nestjs/testing';
import {TypeOrmModule} from '@nestjs/typeorm';
import {TariffService} from './tariff.service';
import {TariffModule} from './tariff.module';
import {DatabaseProvider} from '../../database.provider';

describe('GameSessionsService', () => {
    let service: TariffService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [TariffModule, TypeOrmModule.forRoot(DatabaseProvider)],
            providers: [TariffService],
        }).compile();

        service = module.get<TariffService>(TariffService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('should calc game session price for tariff', async () => {
        let sessionTime = 480;
        expect(await service.calcPriceForTariffBySessionTime(1, new Date('2019-01-01T23:30'), sessionTime)).toBe(182.5);
        sessionTime = 10;
        expect(await service.calcPriceForTariffBySessionTime(1, new Date('2019-01-01T23:55'), sessionTime)).toBe(4.17);
        sessionTime = 90;
        expect(await service.calcPriceForTariffBySessionTime(1, new Date('2019-01-01T12:30'), sessionTime)).toBe(42.5);
    });
});
