import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {Tariff} from './tariff.entity';
import moment = require('moment');
import {TariffInterval} from '../tariff-interval/tariff-interval.entity';
import {start} from 'repl';

const MINUTES_IN_HOUR = 60;

@Injectable()
export class TariffService {
    constructor(
        @InjectRepository(Tariff)
        private readonly tariffRepository: Repository<Tariff>,
    ) {}

    async findAll(): Promise<Tariff[]> {
        return await this.tariffRepository.find();
    }

    async getOneWithRelations(tariffId: number): Promise<Tariff> {
        return this.tariffRepository.findOne({
            relations: ['intervals'],
            where: {id: tariffId},
        });
    }

    async calcPriceForTariffBySessionTime(tariffId: number, startDate: Date, sessionTime: number): Promise<number> {
        const tariff = await this.getOneWithRelations(tariffId);
        let price = 0;
        const startDateMoment = moment(startDate);

        const minutesToEndOfCurrentHour = 60 - startDateMoment.get('minutes');
        const currentHourDate = new Date(startDate);
        currentHourDate.setMinutes(0);
        currentHourDate.setSeconds(0);

        const intervalBetweenNowAndEndOfHour = this.getIntervalForHourFromIntervalList(currentHourDate, tariff.intervals);
        price += intervalBetweenNowAndEndOfHour.price / MINUTES_IN_HOUR * minutesToEndOfCurrentHour;

        sessionTime -= minutesToEndOfCurrentHour;
        startDateMoment.add(minutesToEndOfCurrentHour, 'minutes');

        const endDateMoment = moment(startDateMoment);
        endDateMoment.add(sessionTime, 'minutes');

        const hoursInSession = endDateMoment.diff(startDateMoment, 'hours');
        const minutesInSession = sessionTime % MINUTES_IN_HOUR;


        for (let i = hoursInSession; i > 0 ; i--) {
            const interval = this.getIntervalForHourFromIntervalList(new Date(startDateMoment.toISOString()), tariff.intervals);
            price += interval.price;
            startDateMoment.add(1, 'hour');
        }

        const intervalForMinutes = this.getIntervalForHourFromIntervalList(new Date(startDateMoment.toISOString()), tariff.intervals);
        price += intervalForMinutes.price / MINUTES_IN_HOUR * minutesInSession;

        return Promise.resolve(parseFloat(price.toFixed(2)));
    }

    private getIntervalForHourFromIntervalList(date: Date, intervals: TariffInterval[]): TariffInterval {
        if (!Array.isArray(intervals)) {
            return;
        }

        let intervalToReturn = null;
        intervals.map(interval => {
            if (this.isTimeBetweenOfInterval(date, interval)) {
                intervalToReturn = interval;
            }
        });

        return intervalToReturn;
    }

    private isTimeBetweenOfInterval(date: Date, interval: TariffInterval): boolean {
        const intervalTimeFromSplitted = interval.from.split(':');
        const timeFrom = new Date(date);
        timeFrom.setHours(parseInt(intervalTimeFromSplitted[0], 10));
        timeFrom.setMinutes(parseInt(intervalTimeFromSplitted[1], 10));
        timeFrom.setMinutes(0);
        timeFrom.setMilliseconds(0);

        const intervalTimeToSplitted = interval.to.split(':');
        const timeTo = new Date(date);
        timeTo.setHours(parseInt(intervalTimeToSplitted[0], 10));
        timeTo.setMinutes(parseInt(intervalTimeToSplitted[1], 10));
        timeTo.setMinutes(0);
        timeTo.setMilliseconds(0);
        return moment(date).isBetween(moment(timeFrom), moment(timeTo), 'hours', '[]');
    }
}
