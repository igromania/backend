import {Column, Entity, PrimaryGeneratedColumn, Table, Unique} from 'typeorm';

@Entity()
@Unique(['login'])
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 35 })
    login: string;

    @Column({ length: 35 })
    password: string;

    @Column({ length: 100 })
    fullName: string;

    @Column({
        type: 'enum',
        enum: [
            'owner',
            'operator',
        ],
        default: 'owner',
    })
    role: string;
}
