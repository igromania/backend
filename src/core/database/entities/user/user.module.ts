import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {User} from './user.entity';
import {UsersService} from './users.service';
import {UsersController} from '../../../controllers/users/users.controller';
import {AuthService} from '../../../services/auth/auth.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([User])],
    providers: [UsersService, AuthService],
    controllers: [UsersController],
})
export class UserModule {
}
