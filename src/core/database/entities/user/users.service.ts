import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {User} from './user.entity';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
    ) {}

    async findAll(): Promise<User[]> {
        return await this.userRepository.find();
    }

    async findByLoginAndPassword(login: string, password: string): Promise<User> {
        // TODO: Make md5
        return await this.userRepository
            .createQueryBuilder()
            .where('login = :login AND password = :password', { login, password })
            .getOne();
    }

    async getByLogin(login?: string) {
        return await this.userRepository
            .createQueryBuilder()
            .where('login = :login', {login})
            .getOne();
    }
}
