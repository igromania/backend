export enum ComputerStatus {
    FREE,
    BUSY,
    OFFLINE,
}
