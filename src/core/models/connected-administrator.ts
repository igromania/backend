import {Client} from 'socket.io';

export class ConnectedAdministrator {
    bearer?: string;
    client?: Client | any;
}
