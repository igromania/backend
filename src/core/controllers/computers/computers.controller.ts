import {Controller, Get} from '@nestjs/common';
import {ComputersService} from '../../database/entities/computer/computers.service';
import {Roles} from '../../decorators/roles.decorator';

@Controller('computers')
export class ComputersController {
    constructor(private readonly computersService: ComputersService) {

    }

    @Get()
    @Roles('owner', 'operator')
    findAll() {
        return this.computersService.findAll();
    }
}
