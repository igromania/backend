import {Body, Controller, Post, Req, Res, Session} from '@nestjs/common';
import {UsersService} from '../../database/entities/user/users.service';
import {CommonResponseDto} from '../../dto/common-response.dto';
import {AuthDto} from '../../dto/auth.dto';
import {Roles} from '../../decorators/roles.decorator';
import {AuthService} from '../../services/auth/auth.service';
import * as passport from 'passport';
import { Response, Request } from 'express';

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService,
                private readonly authService: AuthService) {

    }

    @Post('auth')
    @Roles()
    async auth(@Body() authDto: AuthDto, @Req() request: Request, @Res() res: Response) {
        const foundUser = await this.usersService.findByLoginAndPassword(authDto.login, authDto.password);
        const authResult = typeof(foundUser) !== 'undefined';
        const response: CommonResponseDto = {
            success: authResult,
        };

        if (authResult) {
            const token = this.authService.createToken({login: foundUser.login});
            (request as any).session.user = foundUser;
            res.cookie('token', token);
            return res.send(response);
        }

        return response;
    }
}
