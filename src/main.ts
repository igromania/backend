import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import * as session from 'express-session';
import * as passport from 'passport';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.use(session({ secret: 'nest is awesome', resave: true, saveUninitialized: false}));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(cookieParser());
    await app.listen(3333);
}

bootstrap();
